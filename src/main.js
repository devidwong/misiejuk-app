// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import VueCordova from 'vue-cordova'
import VueHead from 'vue-head'
// polyfills
import 'es6-promise/auto' // before vuex
// firebase bindings
import Vuefire from 'vuefire'
// vueJS modules
import App from './App'
import { sync } from 'vuex-router-sync'
import router from './router'
import store from './store'
// helpers
import { FBApp, FBUIApp, FBDb } from './helpers/initFirebase.js'

Vue.use(Vuetify)
Vue.config.productionTip = false
Vue.use(VueCordova)
Vue.use(VueHead)
Vue.use(Vuefire)

sync(store, router)

FBApp.auth().onAuthStateChanged(user => {
  store.commit('SET_USER', user)
  /*
  if (user) {
    FBDb.ref('/users/' + user.uid + '/roles/admin').once('value').then((snapshot) => {
      store.commit('SET_PERMISSIONS', snapshot.val())
    })
  }
  */

  /* eslint-disable no-new */
  new Vue({
    el: '#app',
    router,
    store,
    template: '<App/>',
    components: { App },
    head: {
      meta: [
        {
          name: 'viewport',
          content: 'width=device-width, initial-scale=1, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no, viewport-fit=cover'
        }
      ]
    }
  })
})
store.commit('SET_FB_APP', FBApp)
store.commit('SET_FB_UI_APP', FBUIApp)
store.commit('SET_FB_DB', FBDb)
