import Vue from 'vue'
import Vuex from 'vuex'
import { firebaseMutations } from 'vuexfire'

const getters = {
  user: state => state.user,
  fbApp: state => state.fbApp,
  fbUiApp: state => state.fbUiApp,
  fbDb: state => state.fbDb
}

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    user: null,
    fbApp: null,
    fbUiApp: null,
    fbDb: null
  },
  actions: {
  },
  mutations: {
    SET_USER (state, user) {
      state.user = user
    },
    SET_FB_APP (state, fbApp) {
      state.fbApp = fbApp
    },
    SET_FB_UI_APP (state, fbUiApp) {
      state.fbUiApp = fbUiApp
    },
    SET_FB_DB (state, fbDb) {
      state.fbDb = fbDb
    },
    ...firebaseMutations
  },
  getters
})

export default store
